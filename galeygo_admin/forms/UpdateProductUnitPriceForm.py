from django.contrib.admin.helpers import ActionForm
from django import forms

class UpdateProductUnitPriceForm(ActionForm):
    unit_price = forms.FloatField()