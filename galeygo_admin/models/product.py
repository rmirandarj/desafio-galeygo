# -*- coding: utf-8 -*-
from django.db import models

class Product(models.Model):
    id = models.IntegerField(db_column='ProductID', primary_key=True)  # Field name made lowercase.
    product_name = models.TextField(db_column='ProductName')    # Field name made lowercase.
    supplier_id = models.IntegerField(db_column='SupplierID', blank=True, null=True)  # Field name made lowercase.
    category_id = models.IntegerField(db_column='CategoryID', blank=True, null=True)  # Field name made lowercase.
    quantity_per_unit = models.CharField(db_column='QuantityPerUnit', max_length=20, blank=True, null=True)  # Field name made lowercase.
    unit_price = models.TextField(db_column='UnitPrice', blank=True, null=True)  # Field name made lowercase. This field type is a guess.
    units_in_stock = models.IntegerField(db_column='UnitsInStock', blank=True, null=True)  # Field name made lowercase.
    units_on_order = models.IntegerField(db_column='UnitsOnOrder', blank=True, null=True)  # Field name made lowercase.
    reorder_level = models.IntegerField(db_column='ReorderLevel', blank=True, null=True)  # Field name made lowercase.
    discontinued = models.IntegerField(db_column='Discontinued')  # Field name made lowercase.

    class Meta:
        app_label = 'galeygo_admin'
        db_table = 'Products'