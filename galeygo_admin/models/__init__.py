# -*- coding: utf-8 -*-
from .region import *
from .product import *
from .customer import *
from .employee import *
