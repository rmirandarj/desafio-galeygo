# -*- coding: utf-8 -*-

from django.db import models


class Employee(models.Model):
    id = models.IntegerField(db_column='EmployeeID', primary_key=True)  # Field name made lowercase.
    last_name = models.CharField(db_column='LastName', max_length=20)  # Field name made lowercase.
    first_name = models.CharField(db_column='FirstName', max_length=10)  # Field name made lowercase.
    title = models.CharField(db_column='Title', max_length=30, blank=True, null=True)  # Field name made lowercase.
    title_of_courtesy = models.CharField(db_column='TitleOfCourtesy', max_length=25, blank=True, null=True)  # Field name made lowercase.
    birth_date = models.DateTimeField(db_column='BirthDate', auto_now_add=True, verbose_name="Birth date") # Field name made lowercase. This field type is a guess.
    hire_date = models.DateTimeField(db_column='HireDate', auto_now_add=True, verbose_name="Hire date")  # Field name made lowercase. This field type is a guess.
    address = models.CharField(db_column='Address', max_length=60, blank=True, null=True)  # Field name made lowercase.
    city = models.CharField(db_column='City', max_length=15, blank=True, null=True)  # Field name made lowercase.
    region = models.CharField(db_column='Region', max_length=15, blank=True, null=True)  # Field name made lowercase.
    postal_code = models.CharField(db_column='PostalCode', max_length=10, blank=True, null=True)  # Field name made lowercase.
    country = models.CharField(db_column='Country', max_length=15, blank=True, null=True)  # Field name made lowercase.
    home_phone = models.CharField(db_column='HomePhone', max_length=24, blank=True, null=True)  # Field name made lowercase.
    extension = models.CharField(db_column='Extension', max_length=4, blank=True, null=True)  # Field name made lowercase.
    photo = models.BinaryField(db_column='Photo', blank=True, null=True)  # Field name made lowercase.
    notes = models.TextField(db_column='Notes', blank=True, null=True)  # Field name made lowercase.
    reports_to = models.IntegerField(db_column='ReportsTo', blank=True, null=True)  # Field name made lowercase.
    photo_path = models.CharField(db_column='PhotoPath', max_length=255, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Employees'