# -*- coding: utf-8 -*-
from django.contrib import admin

from galeygo_admin.models import Customer
from django.db.models.functions import Length


class CountryListFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = ('Country')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'country'

    def lookups(self, request, model_admin):
        list_tuple = []
        for c in Customer.objects.raw('SELECT DISTINCT 1 CustomerID, Country FROM Customers WHERE LENGTH(Country) <= 3'):
            list_tuple.append((c.country, c.country))

        return tuple(list_tuple)

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(country=self.value())
        else:
            return queryset


class CustomerAdmin(admin.ModelAdmin):
    model = Customer
    list_display = ('id', 'company_name', 'country', 'region')
    list_filter = [CountryListFilter, 'region']
    readonly_fields = ['id']