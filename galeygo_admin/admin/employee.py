# -*- coding: utf-8 -*-
from django.contrib import admin

from galeygo_admin.models import Employee

class EmployeeAdmin(admin.ModelAdmin):
    model = Employee
    list_display = ('id', 'first_name', 'hire_date')
    list_filter = ['hire_date']
    readonly_fields = ['id']