# -*- coding: utf-8 -*-
from django.contrib import admin
from galeygo_admin.models import Product
from galeygo_admin.forms import  UpdateProductUnitPriceForm
from django.contrib import admin
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect
from django.contrib import messages

def action_product(self, request, queryset):
   newPrice = float(request.POST['unit_price'])
   queryset.update(unit_price=newPrice)
   messages.success(request, '{0} movies were updated'.format(queryset.count()))

action_product.short_description = 'Change unit price'



class ProductAdmin(admin.ModelAdmin):
    model = Product
    list_display = ('id', 'product_name', 'unit_price')
    action_form = UpdateProductUnitPriceForm
    actions = [action_product]
    readonly_fields = ['id']


