
def main():

    print(is_palindrome("A miss é péssima"))


def is_palindrome(word):

    reversed_word = str(word[::-1])

    return word.lower().replace(' ', '') == reversed_word.lower().replace(' ', '')


if __name__ == '__main__':
    main()