from stone_age.exercise_4 import is_palindrome


def main():

    print(return_the_greater_palindrome_product_of_2_numbers_of_3_digits())


def return_the_greater_palindrome_product_of_2_numbers_of_3_digits():

    for i in range(999, 100, -1):

        for j in range(999, 100, -1):

            product = i * j

            if is_palindrome(str(product)):
                return product

    return -1


if __name__ == '__main__':
    main()