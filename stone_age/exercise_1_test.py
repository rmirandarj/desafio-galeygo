import unittest
from stone_age.exercise_1 import return_max_value_from_list


class TestMaxValue(unittest.TestCase):

    def teste_deve_retornar_o_maior_valor_da_lista_neste_caso_1000(self):
        self.assertEqual(return_max_value_from_list([4, 7, -1, 99, 1000, 1]), 1000)


if __name__ == '__main__':
    unittest.main()