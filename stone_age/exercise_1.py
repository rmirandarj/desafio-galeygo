import sys


def main():
    int_list = [4, 7, 45, 99, 10000, 1]
    print(return_max_value_from_list(int_list))


def return_max_value_from_list(int_list):

    max_value = -sys.maxsize - 1

    for number in int_list:
        if number > max_value:
            max_value = number

    return max_value


if __name__ == '__main__':
    main()
