

def main():

    print(return_list_with_interleaved_elements(['a', 'b', 'c', 'd', 'e'], [1, 2, 3]))


def return_list_with_interleaved_elements(l1, l2):

    l3 = []

    if len(l1) >= len(l2):

        for i in range(0, len(l1)):

            l3.append(l1[i])

            if i < len(l2):
                l3.append(l2[i])

    else:

        for i in range(0, len(l2)):

            if i < len(l1):
                l3.append(l1[i])

            l3.append(l2[i])

    return l3


if __name__ == '__main__':
    main()
