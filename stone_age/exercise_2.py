
def main():

    print(calculate_sum_of_multiples_of_3_from_1_to_n(10))


def calculate_sum_of_multiples_of_3_from_1_to_n(n):

    total = 0

    for number in range(1, n + 1):

        if number % 3 == 0:
            total = total + number

    return total


if __name__ == '__main__':
    main()